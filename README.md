# Factorial App
A very simple Python application, which calculates the factorial of given positive number over the http get request.

### Prerequisites
To able to work on this project:
- AWS account should be configured.
- The AWS CLI installed.
- The Docker installed.

### Configuration
The following environment variable can be configured:
| Variable name | Example value | Description |
|---|---|---|
| PORT | 8080 | This port will be used while starting web server. |

### Publishing Docker Images:
This project uses a private ECR repository. Building and publish images is not automated since this is just a case study. Check following steps to build and publish an image manually.

Repository url should be `901077794876.dkr.ecr.us-east-1.amazonaws.com/factorial`.
But you can validate it by running the following command in terraform repository:
```sh
terraform output -json factorial_app
```
To login ECR registry you can run:
```sh
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin {{ecr_url}}
```
To build and push the docker image:
```sh
docker build . -t {{ecr_url}}:{{version}}
docker push {{ecr_url}}:{{version}}
```
