FROM python:3.9.7
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
RUN mkdir /app/
COPY factorial.py /app/
WORKDIR /app/
CMD ["python", "/app/factorial.py"]
