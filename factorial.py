from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse, parse_qs
from io import BytesIO
import os


def factorial(n):
    """Returns factorial of given integer"""
    result = 1
    for i in range(1, n + 1):
        result = result * i
    return result


def factorial_limited(n):
    """Returns factorial of given integer, until it reach to sys.getrecursionlimit()"""
    return 1 if n == 0 else n * factorial_limited(n - 1)


def validate(n):
    """Returns True, if n can be a positive integer."""
    try:
        return True if int(n) >= 0 else False
    except ValueError:
        return False


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        """Simple GET method handler which calculates of the factorial of given positive number"""
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        parsed_query = parse_qs(urlparse(self.path).query)
        number = parsed_query.get('n', False)
        response = BytesIO()
        # query's values are always list since it's possible to set many like: ?n=4&n=9
        if number and validate(number[0]):
            positive_int = int(number[0])
            response.write(f'Factorial for {positive_int} is {factorial(positive_int)}'.encode())
        else:
            response.write(b'You can GET the factorial result of given positive number by querying like: ?n=4')
        self.wfile.write(response.getvalue())


# Create server object and starts web server
port = int(os.environ.get("PORT", 80))
server_object = HTTPServer(('', port), SimpleHTTPRequestHandler)
server_object.serve_forever()
